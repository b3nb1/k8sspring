## Spring Boot CRUD Kubernetes (Minikube) Tutorial
###### This is a small Spring Boot CRUD app encoupled on MariaDB for my own learning purposes.
###### So the first approach is to set up a simple (really not fancy) CRUD appilcation.
  1. Should be enough to execute and test the app by executing `./mvnw spring-boot:run`
  2. And open `http://localhost:8080` in the browser
###### Next step is to Dockerize the application and the database. Here i modified the pom.xml to automatically build the Docker container.
  1. Execute `./mvnw clean package` to build the Docker container, as defined in pom.xml and also the `.jar` file.
  2. This should not affect local testing as it is described above
  3. Optional: run a local mariadb container and link it with the spring boot app container for lokal testing.
###### Set up Minikube and run this app within a local kubernetes cluster.
  1. Installing minikube and kubectl
  2. Start minikube `$minikube start`
  3. Get info `$kubectl cluster-info`
###### Fix the Mariadb properties for production
  1. Add `/app/src/main/resources/application-prod.properties` and add production config
###### Implement Minikube config
  1. Implement the pods by implementing the `.yml` files in the `/kube` folder. 
  2. In this approach i created the database config for mariadb by typing:
    ```
    $ kubectl create secret generic mysql-root-pass --from-literal=password=root
    $ kubectl create secret generic mysql-user-pass --from-literal=username=spring --from-literal=password=password
    $ kubectl create secret generic mysql-db-url --from-literal=database=database --from-literal=url='jdbc:mariadb://database:3306/database?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false'
    ```

  3. This is not very clever for automatisation approach, in the future this should be fixed by holding the parameters in an `.env` file or something like that
  4. Apply the application: `$kubectl apply -f kube`
  5. Check if all is running `$kubectl get pods --watch`
  6. Execute app in browser by executing the service layer from the app: `$minikube service springbootcrud`

###### Create Ingress controller
  1. `kubectl create -f ingress.yml (creates a new ressource)`
  2. `minikube addons enable ingress`

###### Add Istio service-mesh and test it locally with Minikube
  1. Start minikube if not running 
  2.  Helm init has been removed from helm 3.0. You don't need it anymore. There is no more Tiller and the client directories are initialised automatically   when you start using helm.
  3. Install Istio
  4. `istioctl manifest apply --set profile=demo` 
  5. check which services has been installed: `kubectl get svc -n istio-system` -> `kubectl get pods -n istio-system`

* ISTIO SET-UP (https://istio.io/latest/docs/examples/bookinfo/)
  0. Getting started: for istioctl set: `export PATH=$PWD/bin:$PATH` in istio directory, after installation.
    `minikube addons enable istio`
  1. Start application services: `kubectl label namespace default istio-injection=enabled`
  2. Deploy application if not yet
  3. Manual sidecar injection: `kubectl apply -f <(istioctl kube-inject -f kube/*)`
  4. Determine ingress ip and port (configure gateway and virtual service) and depoly (`kubectl apply -f ...`)
  5. Apply destination rules depoly (`kubectl apply -f ...`)
  6. Check if application is healthy with: `istioct l analyze` and `istioctl dashboard kiali`
  7. Host: k8sspringbootcruddemo.info

* Monitoring
  1. After setting gateway, virtual service and destination rules (https://istio.io/latest/docs/examples/bookinfo/)
  2. `export GATEWAY_URL=$INGRESS_HOST:$INGRESS_PORT`
  3. create traffic: `curl http://$GATEWAY_URL/k8sspringbootcruddemo.info`
  4. `kubectl -n istio-system port-forward $(kubectl -n istio-system get pod -l app=grafana -o jsonpath='{.items[0].metadata.name}') 3000:3000 &`
  5. Open Grafan board: http://localhost:3000/dashboard/db/istio-mesh-dashboard
  6. Querying metrics with prometheus: istioctl dashboard prometheus

###### Set up Jenkins CI/CD pipeline
  1. Check if RBAC (Role Based Access Control) ist enabled: `kubectl cluster-info dump | grep authorization-mode`
  2. Add Jenkins file and push to git
  3. `helm repo add codecentric https://codecentric.github.io/helm-charts`
  4. `helm install jenkins-ci codecentric/jenkins --version 1.7.0`
  5. Port forwarding to access jenkins on http://127.0.0.1:8080: `export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=jenkins,app.kubernetes.io/instance=jenkins-ci" -o    jsonpath="{.items[0].metadata.name}")`, `kubectl port-forward --namespace default "$POD_NAME" 8080:8080`
  6. Retrieve the initial password: `export POD_NAME=$(kubectl get pods --namespace default -l "app.kubernetes.io/name=jenkins,app.kubernetes.io/instance=jenkins-ci" -o jsonpath="{.items[0].metadata.name}")`, `kubectl exec --namespace default "$POD_NAME" cat /var/jenkins_home/secrets/initialAdminPassword`
  7. Credetials: uname: admin, pw: admin, name: admin, email: benjamin.bajorat@m...
  8. Install Blue Ocean for Jenkins
  9. 
  
 
###### Extend app features (Frontend, Backend), get rid of thymeleaf (use something more extended)